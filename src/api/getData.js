import fetch from '@/utils/axios-fetch'

/**
 * 获取图片验证码
 */
export const captchaImg = urlparam => fetch('/manager/captcha/img?' + urlparam, {}, 'POST');
// export const captchaImg = data => fetch('/manager/captcha/img', data, 'POST');

/**
 * 获取登陆用户头像
 */
export const avatarImg = urlparam => fetch('/manager/image/avatar?' + urlparam, {}, 'POST');


/**
 * 头像数据删除
 */
export const avatarImgDelete = data => fetch('/manager/image/avatarImgDelete',data, 'POST');


/**
 * 获取手机验证码
 */
export const smsCode = urlparam => fetch('/manager/code/sms?' + urlparam, {}, 'POST');

/**
 * 用户登录
 */
export const login = urlparam => fetch('/manager/login?' + urlparam, {}, 'POST');

/**
 * 退出登录、注销
 */
export const logout = data => fetch('/manager/logout', data, 'POST');

/**
 * 查询当前登录用户
 */
export const getUserInfo = () => fetch('/manager/user/current', {}, 'GET');

/**
 * 查询当前登录用户对应AdUser数据
 */
export const getAdUserInfo = () => fetch('/manager/user/currentAdUser', {}, 'GET');


/**
 * 修改登录用户密码
 */
export const changePwd = urlparam => fetch('/manager/user/changePwd?' + urlparam, {}, 'POST');

/**
 * 查询所有用户信息
 */
export const userListQuery = data => fetch('/manager/user/list/query', data, 'GET');

/**
 * 查询所有用户信息
 */
export const userListQueryParam = data => fetch('/manager/user/list/queryParam', data, 'GET');


/**
 * 删除用户
 */
export const userDelete = data => fetch('/manager/user/delete', data, 'DELETE');

/**
 * 创建DMS用户信息
 */
export const userDMSCreate = data => fetch('/manager/user/dms/create', data, 'POST');

/**
 * 创建IMS用户信息
 */
export const userIMSCreate = data => fetch('/manager/user/ims/create', data, 'POST');

/**
 * 修改用户信息
 */
export const userUpdate = data => fetch('/manager/user/update', data, 'PUT');

/**
 * 保存用户头像信息
 */
export const updateUserAvatar = data => fetch('/manager/user/updateUserAvatar', data, 'PUT');


/**
 * 查询平台号码信息
 */
export const numberSelect = data => fetch('/manager/terrace/number/select', data, 'GET');

/**
 * 查询开销户成功率
 */
export const numberRate = data => fetch('/manager/terrace/number/success/rate/select', data, 'GET');

/**
 * 创建区号合并
 */
export const mergeCreate = data => fetch('/manager/merge/create', data, 'POST');

/**
 * 删除区号合并
 */
export const mergeDelete = data => fetch('/manager/merge/delete', data, 'DELETE');

/**
 * 查询区号合并
 */
export const mergeQuery = data => fetch('/manager/merge/query', data, 'GET');

/**
 * 修改区号合并
 */
export const mergeUpdate = data => fetch('/manager/merge/update', data, 'PUT');

/**
 * 删除业务签约信息
 */
export const signedDelete = data => fetch('/manager/business/signed/delete', data, 'DELETE');

/**
 * 查询业务签约信息
 */
export const signedQuery = data => fetch('/manager/business/signed/query', data, 'GET');

/**
 * 修改业务签约信息
 */
export const signedUpdate = data => fetch('/manager/business/signed/update', data, 'POST');

/**
 * 查询省份列表信息
 */
export const provinceQuery = data => fetch('/manager/province/query', data, 'GET');

/**
 * 查询redis省份大区信息
 */
export const provinceRedis = data => fetch('/manager/province/redisProvinceList', data, 'GET');

/**
 * 修改省份
 */
export const provinceUpdate = data => fetch('/manager/province/update', data, 'POST');

/**
 * 创建大区
 */
export const regionCreate = data => fetch('/manager/region/create', data, 'POST');

/**
 * 删除大区
 */
export const regionDelete = data => fetch('/manager/region/delete', data, 'DELETE');

/**
 * 查询大区
 */
export const regionQuery = data => fetch('/manager/region/query', data, 'GET');

/**
 * 修改大区
 */
export const regionUpdate = data => fetch('/manager/region/update', data, 'PUT');

/**
 * 大区省份配置查询
 */
export const provinceConfigQuery = data => fetch('/manager/province/config/query', data, 'GET');

/**
 * 大区省份配置修改
 */
export const provinceConfigUpdate = data => fetch('/manager/province/config/update', data, 'PUT');

/**
 * DMS注册用户查询
 */
export const singQuery = data => fetch('/manager/sing/query', data, 'GET');

/**
 * DMS终端日志查询
 */
export const logsQuery = data => fetch('/manager/logs/query', data, 'GET');

/**
 * 测试用抛错
 */
export const testErr = () => fetch('/manager/user/testErr', {}, 'GET');

/**
 * 新增DMS角色
 */
export const addDmsRole = data => fetch('/manager/permission/addDmsRole', data, 'POST');

/**
 * 批量保存权限菜单信息
 */
export const batchSaveRolePermission = data => fetch('/manager/permission/batchSaveRolePermission', data, 'POST');

/**
 * 获取DMS角色
 */
export const getDmsRole = data => fetch('/manager/permission/getDmsRole', data, 'GET');

/**
 * 获取模块信息
 */
export const listAllModules = data => fetch('/manager/permission/listAllModules', data, 'GET');

/**
 * 获取当前用户可配置模块信息
 */
export const listModules = data => fetch('/manager/permission/listModules', data, 'GET');

/**
 * 获取角色列表信息
 */
export const listAllRoles = data => fetch('/manager/permission/listAllRoles', data, 'GET');


/**
 * 获取当前用户可配置菜单信息
 */
export const listMenusUsr = data => fetch('/manager/permission/listMenusUsr', data, 'GET');

/**
 * 获取指定角色菜单信息
 */
export const listMenusTar = data => fetch('/manager/permission/listMenusTar', data, 'GET');

/**
 * 修改DMS角色
 */
export const updateDmsAdRole = data => fetch('/manager/permission/updateDmsAdRole', data, 'POST');

/**
 * 删除DMS角色
 */
export const roleDelete = data => fetch('/manager/permission/deleteDmsRole', data, 'DELETE');

/**
 *获取磁盘监控-主机IP列表
 */
export const hostIpList = data => fetch('/manager/ws/hostIp/list', data, 'GET');
