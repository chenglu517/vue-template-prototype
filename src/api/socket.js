let websock = null;
let global_callback = null;
let serverPort = '8081';	//webSocket连接端口

function getWebsock()
{
  if(null!=websock)
  {
    return websock;
  }
  else
  {
    return null;
  }
}

function isJSON(str) {
  if (typeof str == 'string') {
    try {
      var obj=JSON.parse(str);
      if(typeof obj == 'object' && obj ){
        return true;
      }else{
        return false;
      }

    } catch(e) {
      console.log('error：'+str+'!!!'+e);
      return false;
    }
  }
  console.log('It is not a string!')
}

function getWebIP(){
  let curIP = 'ws://'+window.location.hostname+':'+serverPort;
  console.log("window.location.hostname -> "+curIP);
  return curIP;
}

function initWebSocket(serverEndPointUrl,func)
{
  //初始化weosocket
  //ws地址
  var wsuri = "ws://" +getWebIP()+ ":" + serverPort+serverEndPointUrl;
  console.log('websocket 初始化方法执行 uri为 -> '+wsuri);
  global_callback = func;
  websock = new WebSocket(wsuri);
  websock.onmessage = function(e){
    websocketonmessage(e);
  }
  websock.onclose = function(e){
    websocketclose(e);
  }
  websock.onopen = function () {
    websocketOpen();
  }

  //连接发生错误的回调方法
  websock.onerror = function () {
    console.log("WebSocket连接发生错误");
  }
}

// 实际调用的方法
function sendSock(agentData,callback){
  global_callback = callback;
  if (websock.readyState === websock.OPEN) {
    //若是ws开启状态
    websocketsend(agentData)
  }else if (websock.readyState === websock.CONNECTING) {
    // 若是 正在开启状态，则等待1s后重新调用
    setTimeout(function () {
      sendSock(agentData,callback);
    }, 1000);
  }else {
    // 若未开启 ，则等待1s后重新调用
    setTimeout(function () {
      sendSock(agentData,callback);
    }, 1000);
  }
}

//数据接收
function websocketonmessage(e){
  console.log(e.data);
  if(isJSON(e.data))
  {
    global_callback(JSON.parse(e.data));
  }
  else{
    return;
  }

}



//数据发送
function websocketsend(agentData)
{
  if(typeof(agentData)==="string")
  {
    websock.send(agentData);
  }
  else
  {
    websock.send(JSON.stringify(agentData));
  }
}

//关闭
function websocketclose(e){
  console.log("连接关闭 -> (" + e.code + ")");
}

function websocketOpen(e){
  console.log("连接成功");
}

//initWebSocket();

export{sendSock,initWebSocket,getWebIP}
