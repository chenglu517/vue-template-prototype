import {
	baseUrl
} from './env'
import axios from 'axios'

axios.defaults.baseURL = baseUrl;
axios.defaults.headers.post['Content-Type'] = 'application/json';

axios.interceptors.response.use((res) => {
	if (res.status >= 200 && res.status < 300) {
		return res;
	}
	return Promise.reject({
		message: '请求异常，请重试',
		err: res
	});
}, (error) => {
	// 网络异常
	let msg = '网络异常，请刷新重试';
	// console.log("+++++++++++++++++++++++++++++++++");
	// console.log(error.response);
	// console.log("+++++++++++++++++++++++++++++++++");
	if (error.response.status == "401") {
		msg = error.response.data.msg;
    sessionStorage.setItem("token", "");
    localStorage.clear();
	}
	if(error.response.status == "504"){

  }
  if(error.response.status == "500"){
    console.log("后台500错误");
  }
	if (error.response.data.msg == "您尚未登录！") {
		window.location.href = "/";
    sessionStorage.setItem("token", "");
    localStorage.clear();
	}

	return Promise.reject({
		message: msg,
    err: error.response
	});
});

export default async (url = '', data = {}, type = 'GET', queryData = {}, showError = true) => {
	type = type.toUpperCase();
	url = url;

	if (type == 'GET' || type == 'DELETE') {
		queryData = data;
	}
	try {
		let token = sessionStorage.getItem("token") ? sessionStorage.getItem("token") : "";
		const response = await axios({
			method: type,
			url: url,
			params: queryData,
			data: data,
			headers: {
				'token': token,
			}
		});
		return response.data
	} catch (error) {
		console.log("====================================")
		console.log(error)
		console.log("====================================")
    console.log('status -> '+error.err.status)
		if (error.err.status == "500") {
			return {
				code: "500",
				msg: "系统异常，请联系管理员处理！",
				data: {}
			}
		}
    if (error.err.status>500&&error.err.status<600) {
      return {
        code: "504",
        msg: "网络异常!",
        data: {}
      }
    }
		let resp = error.err && error.err.data ? error.err.data : {
			code: error.err.status,
			message: error.err.statusText
		};
		if (resp.code == "401") {
			return {
				code: resp.code,
				msg: resp.msg,
				data: {}
			}
		}
		if (resp.status == "404") {
			return {
				code: resp.status,
				msg: "请求地址不存在，请确认后刷新重试!",
				data: {}
			}
		}
		if (resp.status == "405") {
			return {
				code: resp.status,
				msg: "错误的请求方式，请确认是否跟接口请求方式一致!",
				data: {}
			}
		}
		return resp;
	}
}
