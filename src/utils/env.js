/**
 * 配置编译环境和线上环境之间的切换
 * 
 * baseUrl: 域名地址
 * routerMode: 路由模式
 */
let baseUrl = '/api'; //跨域代理路径
let routerMode = 'history';

export {
	baseUrl,
	routerMode,
}
