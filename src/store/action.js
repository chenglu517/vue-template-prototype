import {
  getUserInfo
} from '@/api/getData'
import * as types from './mutation-types'

export default {
  async getUserInfox({
                       commit
                     }) {
    try {
      const res = await getUserInfo()
      if (res.code == "200") {
        commit(types.SAVE_ADMIN_INFO, res.data);
      } else {
        commit(types.SAVE_ADMIN_INFO, {});
      }
    } catch (err) {
      console.log('您尚未登陆或者session失效');
      commit(types.SAVE_ADMIN_INFO, {});
    }
  },
  clearUserInfo({
                  commit
                }) {
    commit(types.SAVE_ADMIN_INFO, {});
  }
}
