import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import mutations from './mutations'
import actions from './action'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

let state = {
  token: "",
  userInfo:
  {
    username: ""
  }
}

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user
  },
  state,
  getters,
  actions,
  mutations
})

export default store
