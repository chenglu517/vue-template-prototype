import * as types from './mutation-types'

export default {
  [types.SAVE_ADMIN_INFO](state, userInfo) {
    state.userInfo = userInfo
  }
}
