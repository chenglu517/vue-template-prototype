import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import VModal from 'vue-js-modal'
import '@/icons' // icon
import '@/permission' // permission control

import axios from 'axios'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$axios = axios

import JsEncrypt from 'jsencrypt/bin/jsencrypt';
let Base64 = require('js-base64').Base64;

//引入socket.js
import * as socketApi from './api/socket'
Vue.prototype.socketApi = socketApi

Vue.prototype.$jsEncrypt = JsEncrypt
Vue.prototype.$base64 = Base64

//这里对不同级别类型的$message做了下封装,方便在各种页面(组件)中使用
Vue.prototype.msgSuccess = function (msg) {
  this.$message({ showClose: true, message: msg, type: "success" });
}

Vue.prototype.msgError = function (msg) {
  this.$message({ showClose: true, message: msg, type: "error" });
}

Vue.prototype.msgInfo = function (msg) {
  this.$message.info(msg);
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true, dynamicDefaults: { clickToClose: true } });
Vue.config.productionTip = false

//引入echarts
import echarts from 'echarts'
Vue.prototype.echarts = echarts

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
