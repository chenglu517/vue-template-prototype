import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/dashboard',
  //   children: [{
  //     path: 'dashboard',
  //     name: 'Dashboard',
  //     component: () => import('@/views/dashboard/index'),
  //     meta: { title: '首页', icon: 'index' }
  //   }]
  // },
  {
    path: '/example',
    component: Layout,
    redirect: '/example/adUserList',
    name: 'Example',
    meta: { title: '系统设置', icon: 'example' },
    children: [
      {
        path: 'adUserList',
        name: 'UserList',
        component: () => import('@/views/table/adUserList'),
        meta: { title: '用户管理', icon: 'table' }
      },
      {
        path: 'globalDict',
        name: 'GlobalDict',
        component: () => import('@/views/table/globalDict'),
        meta: { title: '全局字典管理', icon: 'user' }
      }
      // ,
      // {
      //   path: 'tree',
      //   name: 'Tree',
      //   component: () => import('@/views/tree/index'),
      //   meta: { title: 'Tree', icon: 'tree' }
      // }
    ]
  },
  {
    path: '/',
    component: Layout,
    redirect: '/main-index',
    name: 'index-page',
    meta: { title: '舆情首页', icon: 'example' },
    children: [
      {
        path: 'main-index',
        name: 'main-index',
        component: () => import('@/views/main-index/index'),
        meta: { title: '首页', icon: 'table' }
      }
    ]
  },
  {
    path: '/preview',
    component: Layout,
    redirect: '/consensus-view',
    name: 'main-consensus',
    meta: { title: '舆情浏览', icon: 'example' },
    children: [
      {
        path: 'consensus-view',
        name: 'consensus-view',
        component: () => import('@/views/main-consensus-view/index'),
        meta: { title: '舆情浏览', icon: 'table' }
      }
    ]
  },
  {
    path: '/pushwarning',
    component: Layout,
    redirect: '/push-warning',
    name: 'main-consensus',
    meta: { title: '推送预警', icon: 'example' },
    children: [
      {
        path: 'push-warning',
        name: 'push-warning',
        component: () => import('@/views/main-push-warning/indexList'),
        meta: { title: '推送预警列表', icon: 'table' }
      }
    ]
  },
  {
    path: '/more',
    component: Layout,
    //redirect: '/areaConsensus',
    name: 'main-more',
    meta: { title: '更多', icon: 'example' },
    children: [
      {
        path: 'area-consensus',
        name: 'area-consensus',
        component: () => import('@/views/main-more/area-consensus/index'),
        meta: { title: '地域舆情', icon: 'table' }
      },
      {
        path: 'my-follow',
        name: 'my-follow',
        component: () => import('@/views/main-more/my-follow/index'),
        meta: { title: '我的关注', icon: 'table' }
      }
    ]
  },
  {
    path: '/monitor',
    component: Layout,
    redirect: '/monitor/process',
    name: 'Monitor',
    meta: { title: '系统监控', icon: 'example' },
    children: [
      {
        path: 'process',
        name: 'Process',
        component: () => import('@/views/table/processMonitor'),
        meta: { title: '进程信息监控', icon: 'table' }
      },
      {
        path: 'file',
        name: 'File',
        component: () => import('@/views/table/fileMonitor'),
        meta: { title: '文件目录监控', icon: 'table' }
      },
      {
        path: 'disk',
        name: 'Disk',
        component: () => import('@/views/table/diskMonitor'),
        meta: { title: '磁盘监控', icon: 'table' }
      },
      // {
      //   path: 'tree',
      //   name: 'Tree',
      //   component: () => import('@/views/tree/index'),
      //   meta: { title: 'Tree', icon: 'tree' }
      // }
    ]
  },
  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'Form', icon: 'form' }
  //     }
  //   ]
  // },

  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index'),
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },

  // {
  //   path: 'external-link',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: '外部连接', icon: 'link' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
